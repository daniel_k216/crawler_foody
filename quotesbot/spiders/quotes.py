import scrapy

class QuotesSpider(scrapy.Spider):
  name = "foody"
  start_urls = open("links/hcm.txt", "r")
  
  def parse(self, response):
    for foody in response.css('div.pn-microsite'):
      avatar = 'N/A';
      audience = 'N/A';
      district = foody.css('div.res-common-add > span:last_child::text').get();

      if len(foody.css('div.audiences::text')) > 0:
        audience = foody.css('div.audiences::text').get().translate(str.maketrans({'\r': '', '\n': '', '\xa0': '', ',': ', ', '-': ''})).strip();

      # Get Avatar is Image or Video 
      if len(foody.css('img.pic-place')) > 0:
        avatar = foody.css('img.pic-place').attrib['src'];

      # Get Avatar is Image or Video 
      if len(foody.css('img.avatar')) > 0:
        avatar = foody.css('img.avatar').attrib['src'];

      # Get cuisine of Restaurant
      cuisines = []
      for cuisine in foody.css('div.cuisines-list > a::text').getall():
        cuisines.append(cuisine.translate(str.maketrans({'\r': '', '\n': '', ',': '', ' ': ''})));

      # Get all images
      images = [];
      for image in foody.css('div.microsite-professional-photo-item > div.microsite-professional-photo-item-img > a > img'):
        images.append(image.css('img').attrib['src'])

      yield {
        'name': foody.css('h1::text').get(),
        'avatar': avatar,
        'category': foody.css('div.category-items > a::text').get(),
        'cuisine': cuisines,
        'audience': audience,
        'address': (', ').join(foody.css('div.res-common-add > span > a > span::text').getall()) + ', ' + district,
        'exellent': foody.css('b.exellent::text').get().replace('\r\n', '').replace(' ', ''),
        'good': foody.css('b.good::text').get().replace('\r\n', '').replace(' ', ''),
        'average': foody.css('b.average::text').get().replace('\r\n', '').replace(' ', ''),
        'bad': foody.css('b.bad::text').get().replace('\r\n', '').replace(' ', ''),
        'images': images,
      }
